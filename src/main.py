from getmac import get_mac_address
from flask import Flask
import json
import netifaces
import requests
from supervisor.device import *
from supervisor.app import *
from supervisor.service import *

app = Flask(__name__)

SUPERVISOR_ADDRESS = 'http://192.168.1.190:48484'
SUPERVISOR_API_KEY = '79e35fbb13e686d69007d8bad360d8e5c6363aaf1059aea5536d358522320a'
APP_ID = '1476402'


@app.route('/')
def hello_world():
    return 'Hello, Welcome to my rpi host!'


def get_net_interface():
    return netifaces.interfaces()[1]


@app.route('/getmac')
def get_mac_address_from_boards():
    """
        Get MAC Address of the current board network adapter running this agent.
        :return:
        """
    try:
        interface = get_net_interface()
        print('network interfaces : ', interface)
        # str_list = interface[1].split("'")
        # if len(str_list) == 1:
        #     net_interface = str_list[0]
        # else:
        #     net_interface = str_list[1]
        net_interface = interface
        mac_address = get_mac_address(interface=net_interface)
    except Exception as err:
        return err
    else:
        return mac_address


@app.route('/device/<request_type>')
def device(request_type):
    if request_type == 'internal':
        device = get_state_of_device(SUPERVISOR_ADDRESS, SUPERVISOR_API_KEY)
        print(json.loads(device)['api_port'])
        return device
    else:
        return get_state_of_device_remotely('myfyrbm6mHBruCqebMOMCdCBomO4hdny', '235d4452ba5afa4025cf90e95ef1b1d9')


@app.route('/reboot/<request_type>')
def reboot(request_type):
    if request_type == 'internal':
        return reboot_device(SUPERVISOR_ADDRESS, SUPERVISOR_API_KEY, True)
    else:
        return reboot_device_remotely('myfyrbm6mHBruCqebMOMCdCBomO4hdny', '235d4452ba5afa4025cf90e95ef1b1d9')


@app.route('/blink/<request_type>')
def blink(request_type):
    if request_type == 'internal':
        return blink_device(SUPERVISOR_ADDRESS, SUPERVISOR_API_KEY)
    else:
        return blink_device_remotely('myfyrbm6mHBruCqebMOMCdCBomO4hdny', '235d4452ba5afa4025cf90e95ef1b1d9')


@app.route('/regenerate/<request_type>')
def regenerate(request_type):
    if request_type == 'internal':
        return regenerate_api_key(SUPERVISOR_ADDRESS, SUPERVISOR_API_KEY)
    else:
        return regenerate_api_key_proxy('myfyrbm6mHBruCqebMOMCdCBomO4hdny', '235d4452ba5afa4025cf90e95ef1b1d9')


@app.route('/app/info')
def app_info():
    return get_app_info(SUPERVISOR_ADDRESS, SUPERVISOR_API_KEY, APP_ID)


@app.route('/appinfo')
def get_entire_app_info():
    return get_entire_app_state(SUPERVISOR_ADDRESS, SUPERVISOR_API_KEY)


@app.route('/supervisor')
def identify_supervisor():
    if check_supervisor(SUPERVISOR_ADDRESS):
        return "OK"
    else:
        return "Failed"


@app.route('/status')
def get_app_status():
    return get_all_status(SUPERVISOR_ADDRESS, SUPERVISOR_API_KEY)


@app.route('/restart_service')
def restart_service():
    service_data = {'serviceName': 'main'}
    service_action = 'restart'
    if switch_single_service(SUPERVISOR_ADDRESS, SUPERVISOR_API_KEY, APP_ID, service_data, service_action):
        return "OK"
    else:
        return "False"


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
