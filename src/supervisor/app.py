import requests
import inspect
from utils.common import logger
from supervisor.exception.viso_agent_exception import VisoRequestError
import json


# use the [POST /v1/apps/:appId/stop] API
def stop_app_container(supervisor_address, balena_supervisor_api_key, app_id):
    """
    :return: When successful, responds with 200 and the Id of the stopped container.
    """
    try:
        response = requests.post(
            '/'.join([supervisor_address, 'v1/apps', app_id, 'stop']),
            params={'apikey': balena_supervisor_api_key},
            headers={'Content-Type': 'application/json'},
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.text
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


def stop_app_container_remotely(auth_token, uuid, app_id):
    """
    :return: When successful, responds with 200 and the Id of the stopped container.
    """
    try:
        response = requests.post(
            '/'.join(['https://api.balena-cloud.com/supervisor/v1/apps', app_id, 'stop']),
            headers={'Content-Type': 'application/json',
                     'Authorization': 'Bearer {}'.format(auth_token)},
            data={'uuid': uuid}
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.text
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


# use the [POST /v1/apps/:appId/start] API
def start_app_container(supervisor_address, balena_supervisor_api_key, app_id):
    """
    :return: When successful, responds with 200 and the Id of the start container.
    """
    try:
        response = requests.post(
            '/'.join([supervisor_address, 'v1/apps', app_id, 'start']),
            params={'apikey': balena_supervisor_api_key},
            headers={'Content-Type': 'application/json'},
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.text
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


def start_app_container_remotely(auth_token, uuid, app_id):
    """
    :return: When successful, responds with 200 and the Id of the stopped container.
    """
    try:
        response = requests.post(
            '/'.join(['https://api.balena-cloud.com/supervisor/v1/apps', app_id, 'start']),
            headers={'Content-Type': 'application/json',
                     'Authorization': 'Bearer {}'.format(auth_token)},
            data={'uuid': uuid}
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.text
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


# use the [GET /v1/apps/:appId] API
def get_app_info(supervisor_address, balena_supervisor_api_key, app_id):
    """
    :return: Returns the application running on the device The app is a JSON object that contains the following:
            appId: The id of the app as per the balenaCloud API.
            commit: Application commit that is running.
            imageId: The docker image of the current application build.
            containerId: ID of the docker container of the running app.
            env: A key-value store of the app's environment variables.
    """
    try:
        response = requests.get(
            '/'.join([supervisor_address, 'v1/apps', app_id]),
            params={'apikey': balena_supervisor_api_key},
            headers={'Content-Type': 'application/json'},
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.content
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


def get_app_info_remotely(auth_token, uuid, app_id):
    try:
        response = requests.post(
            '/'.join(['https://api.balena-cloud.com/supervisor/v1/apps', app_id]),
            headers={'Content-Type': 'application/json',
                     'Authorization': 'Bearer {}'.format(auth_token)},
            data={'uuid': uuid}
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.content
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


# use the [GET /v1/healthy] API
def check_supervisor(supervisor_address):
    """
    Used internally to check whether the supervisor is running correctly, according to some heuristics that help
    determine whether the internal components, application updates and reporting to the balenaCloud API are
    functioning.

    :return: Responds with an empty 200 response if the supervisor is healthy, or a 500 status code if something is
    not working correctly.
    """
    try:
        response = requests.get('/'.join([supervisor_address, 'v1/healthy']))
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return True
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


def check_supervisor_remotely(auth_token, uuid):
    try:
        response = requests.post(
            'https://api.balena-cloud.com/supervisor/v1/healthy',
            headers={'Content-Type': 'application/json',
                     'Authorization': 'Bearer {}'.format(auth_token)},
            data={'uuid': uuid, 'method': 'GET'}
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return True
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


# use the [GET /v2/applications/state] API
def get_entire_app_state(supervisor_address, balena_supervisor_api_key):
    """
    Get a list of applications, services and their statuses.

    :return: This will reflect the current state of the supervisor, and not the target state.
    """
    try:
        response = requests.get(
            '/'.join([supervisor_address, 'v2/applications/state']),
            params={'apikey': balena_supervisor_api_key},
            headers={'Content-Type': 'application/json'},
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.content
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


def get_entire_app_state_remotely(auth_token, uuid):
    try:
        response = requests.post(
            'https://api.balena-cloud.com/supervisor/v2/applications/state',
            headers={'Content-Type': 'application/json',
                     'Authorization': 'Bearer {}'.format(auth_token)},
            data={'uuid': uuid, "method": "GET"}
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.text
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


# use the [GET /v1/apps/:appId] API
def get_state_of_app(supervisor_address, balena_supervisor_api_key, app_id):
    """
    Use this endpoint to get the state of a single application, given the appId.
    """
    try:
        response = requests.post(
            '/'.join([supervisor_address, 'v2/applications', app_id, 'state']),
            params={'apikey': balena_supervisor_api_key},
            headers={'Content-Type': 'application/json'},
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.content
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


# use the [GET /v2/state/status] API
def get_all_status(supervisor_address, balena_supervisor_api_key):
    """
    This will return a list of images, containers, the overall download progress and the status of the state engine.
    """
    try:
        response = requests.get(
            '/'.join([supervisor_address, 'v2/state/status']),
            params={'apikey': balena_supervisor_api_key},
            headers={'Content-Type': 'application/json'},
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.content
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()
