import requests
from utils.common import logger


class VisoRequestError(requests.exceptions.ConnectionError):

    def __init__(self, err_origin, err_content, err_type='Error'):
        self.err_type = err_type
        self.err_origin = err_origin
        self.err_content = err_content

    def log(self):
        if self.err_type == 'Error':
            logger.error('Error occurred to run <{}> :   Error message - {}'.format(self.err_origin, self.err_content))
            print('Error occurred to run <{}> :   Error message - {}'.format(self.err_origin, self.err_content))
        else:
            logger.error('Failed to run <{}> :   Response Status code - {}'.format(self.err_origin, self.err_content))
            print('Failed to run <{}> :   Response Status code - {}'.format(self.err_origin, self.err_content))
