import requests
import inspect
from utils.common import logger
from supervisor.exception.viso_agent_exception import VisoRequestError


class device():
    def __init__(self):
        self.name = "device"


def ping(supervisor_address, balena_supervisor_api_key):
    """

        :param supervisor_address: The full address for the API, i.e. "http://127.0.0.1:48484"
        :param balena_supervisor_api_key: Apikey parameter, which is exposed to the application
        :return: Responds with an empty 200 response
    """
    try:
        response = requests.get(
            '/'.join([supervisor_address, 'ping']),
            params={'apikey': balena_supervisor_api_key},
            headers={'Content-Type': 'application/json'},
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return True
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


def ping_remotely(auth_token, uuid):
    """

        :param auth_token: Auth Token
        :param uuid: Device ID
        :return:
    """
    try:
        response = requests.post(
            'https://api.balena-cloud.com/supervisor/ping',
            headers={'Content-Type': 'application/json',
                     'Authorization': 'Bearer {}'.format(auth_token)},
            data={'uuid': uuid,
                  'method': 'GET'}
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return True
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


def blink_device(supervisor_address, balena_supervisor_api_key):
    """
        Starts a blink pattern on a LED for 15 seconds, if your device has one

        :param supervisor_address: The full address for the API, i.e. "http://127.0.0.1:48484"
        :param balena_supervisor_api_key: Apikey parameter, which is exposed to the application
        :return: Responds with an empty 200 response
    """
    try:
        response = requests.get(
            '/'.join([supervisor_address, 'blink']),
            params={'apikey': balena_supervisor_api_key},
            headers={'Content-Type': 'application/json'},
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return True
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


def blink_device_remotely(auth_token, uuid):
    """
        Starts a blink pattern on a LED for 15 seconds, if your device has one

        :param auth_token: Auth Token
        :param uuid: Device ID
        :return:
    """
    try:
        response = requests.post(
            'https://api.balena-cloud.com/supervisor/v1/blink',
            headers={'Content-Type': 'application/json',
                     'Authorization': 'Bearer {}'.format(auth_token)},
            data={'uuid': uuid}
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return True
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


# use the [update] API
def update_device(supervisor_address, balena_supervisor_api_key, force_param):
    """
    Triggers an update check on the supervisor. Optionally, forces an update when updates are locked.
    Responds with an empty 204 (No Content) response.

    :param supervisor_address: The full address for the API, i.e. "http://127.0.0.1:48484"
    :param balena_supervisor_api_key: Apikey parameter, which is exposed to the application
    :param force_param: Optionally, forces an update when updates are locked.
    :return:
    """
    try:
        response = requests.post(
            '/'.join([supervisor_address, 'v1/update']),
            params={'apikey': balena_supervisor_api_key},
            headers={'Content-Type': 'application/json'},
            data={'force': force_param}
        )
        if response.status_code != 204:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return True
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


def update_device_remotely(auth_token, uuid, force_param):
    """
        Triggers an update check on the supervisor. Optionally, forces an update when updates are locked.
        Responds with an empty 204 (No Content) response.

        :param auth_token: Auth Token
        :param uuid: Device ID
        :param force_param: Optionally, forces an update when updates are locked.
        :return:
        """
    try:
        response = requests.post(
            'https://api.balena-cloud.com/supervisor/v1/update',
            headers={'Content-Type': 'application/json',
                     'Authorization': 'Bearer {}'.format(auth_token)},
            data={'uuid': uuid, 'data': {'force': force_param}}
        )
        if response.status_code != 204:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return True
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


# use the [reboot] API
def reboot_device(supervisor_address, balena_supervisor_api_key, force_param=False):
    """
    Reboots the device. This will first try to stop applications, and fail if there is an update lock.

    :param supervisor_address: The full address for the API, i.e. "http://127.0.0.1:48484"
    :param balena_supervisor_api_key: Apikey parameter, which is exposed to the application
    :param force_param: An optional "force" parameter in the body overrides the lock when true (and the lock can also be
            overridden from the dashboard)
    :return: When successful, responds with 202 accepted and a JSON object
            { "Data": "OK", "Error": "" }
    """
    try:
        response = requests.post(
            '/'.join([supervisor_address, 'v1/reboot']),
            params={'apikey': balena_supervisor_api_key},
            headers={'Content-Type': 'application/json'},
            # data={'force': True}
        )
        if response.status_code != 202:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.content
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


def reboot_device_remotely(auth_token, uuid):
    """
    Reboots the device. This will first try to stop applications, and fail if there is an update lock.

    :param auth_token: Auth Token
    :param uuid: Device ID
    :return: When successful, responds with 202 accepted and a JSON object
            { "Data": "OK", "Error": "" }
    """
    try:
        response = requests.post(
            'https://api.balena-cloud.com/supervisor/v1/reboot',
            headers={'Content-Type': 'application/json',
                     'Authorization': 'Bearer {}'.format(auth_token)},
            data={'uuid': uuid}
        )
        if response.status_code != 202:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.content
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


# use the [shutdown] API
def shutdown_device(supervisor_address, balena_supervisor_api_key, force_param):
    """
    Dangerous. Shuts down the device. This will first try to stop applications, and fail if there is an update lock.
    An optional "force" parameter in the body overrides the lock when true (and the lock can also be overridden from
    the dashboard).

    :param supervisor_address: The full address for the API, i.e. "http://127.0.0.1:48484"
    :param balena_supervisor_api_key: Apikey parameter, which is exposed to the application
    :param force_param: An optional "force" parameter in the body overrides the lock when true (and the lock can also be
            overridden from the dashboard)
    :return: When successful, responds with 202 accepted and a JSON object
            { "Data": "OK", "Error": "" }
    """
    try:
        response = requests.post(
            '/'.join([supervisor_address, 'v1/shutdown']),
            params={'apikey': balena_supervisor_api_key},
            headers={'Content-Type': 'application/json'},
            data={'force': force_param}
        )
        if response.status_code != 202:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.content
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


def shutdown_device_remotely(auth_token, uuid):
    """
    Dangerous. Shuts down the device. This will first try to stop applications, and fail if there is an update lock.
    An optional "force" parameter in the body overrides the lock when true (and the lock can also be overridden from
    the dashboard).

    :param auth_token: Auth Token
    :param uuid: Device ID
    :return: When successful, responds with 202 accepted and a JSON object
            { "Data": "OK", "Error": "" }
    """
    try:
        response = requests.post(
            'https://api.balena-cloud.com/supervisor/v1/shutdown',
            headers={'Content-Type': 'application/json',
                     'Authorization': 'Bearer {}'.format(auth_token)},
            data={'uuid': uuid}
        )
        if response.status_code != 202:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.content
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


# use the [purge] API
def purge_device(supervisor_address, balena_supervisor_api_key, app_id):
    """
    Clears the user application's /data folder.

    :param supervisor_address: The full address for the API, i.e. "http://127.0.0.1:48484" :param
    :param balena_supervisor_api_key: Apikey parameter, which is exposed to the application
    :param app_id: Has to be a JSON
    object with an appId property, corresponding to the ID of the application the device is running. :return: WWhen
    successful, responds with 200 and a JSON object: { "Data": "OK", "Error": "" }
    """
    try:
        response = requests.post(
            '/'.join([supervisor_address, 'v1/purge']),
            params={'apikey': balena_supervisor_api_key},
            headers={'Content-Type': 'application/json'},
            data={'appId': app_id}
        )
        if response.status_code != 202:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.content
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


def purge_device_remotely(auth_token, uuid, app_id):
    """
    Clears the user application's /data folder.

    :param auth_token: Auth Token
    :param uuid: Device ID
    :param app_id: Has to be a JSON object with an appId
    object with an appId property, corresponding to the ID of the application the device is running. :return: WWhen
    successful, responds with 200 and a JSON object: { "Data": "OK", "Error": "" }
    """
    try:
        response = requests.post(
            'https://api.balena-cloud.com/supervisor/v1/purge',
            headers={'Content-Type': 'application/json',
                     'Authorization': 'Bearer {}'.format(auth_token)},
            data={'uuid': uuid, 'app_id': app_id}
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.content
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


# use the [restart] API
def restart_device(supervisor_address, balena_supervisor_api_key, app_id):
    """
    Restarts a user application container

    :param supervisor_address: The full address for the API, i.e. "http://127.0.0.1:48484"
    :param balena_supervisor_api_key: Apikey parameter, which is exposed to the application
    :param app_id: Has to be a JSON
    object with an appId property, corresponding to the ID of the application the device is running. :return: When
    successful, responds with 200 and an "OK"
    """
    try:
        response = requests.post(
            '/'.join([supervisor_address, 'v1/restart']),
            params={'apikey': balena_supervisor_api_key},
            headers={'Content-Type': 'application/json'},
            data={'appId': app_id}
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.text
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


def restart_device_remotely(auth_token, uuid, app_id):
    """
    Restarts a user application container

    :param auth_token: Auth Token
    :param uuid: Device ID
    :param app_id: Has to be a JSON object with an appId
    :return: When successful, responds with 200 and an "OK"
    """
    try:
        response = requests.post(
            'https://api.balena-cloud.com/supervisor/v1/restart',
            headers={'Content-Type': 'application/json',
                     'Authorization': 'Bearer {}'.format(auth_token)},
            data={'uuid': uuid, 'app_id': app_id}
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.text
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


# use the [regenerate-api-key] API
def regenerate_api_key(supervisor_address, balena_supervisor_api_key):
    """
    Invalidates the current BALENA_SUPERVISOR_API_KEY and generates a new one. Responds with the new API key,
    but the application will be restarted on the next update cycle to update the API key environment variable.

    :param supervisor_address: The full address for the API, i.e. "http://127.0.0.1:48484"
    :param balena_supervisor_api_key: Apikey parameter, which is exposed to the application
    :return: new API KEY
    """
    try:
        response = requests.post(
            '/'.join([supervisor_address, 'v1/regenerate-api-key']),
            params={'apikey': balena_supervisor_api_key},
            headers={'Content-Type': 'application/json'},
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.text
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


def regenerate_api_key_remotely(auth_token, uuid):
    """
    Invalidates the current BALENA_SUPERVISOR_API_KEY and generates a new one. Responds with the new API key,
    but the application will be restarted on the next update cycle to update the API key environment variable.

    :param auth_token: Auth Token
    :param uuid: Device ID
    :return: new API KEY
    """
    try:
        response = requests.post(
            'https://api.balena-cloud.com/supervisor/v1/regenerate-api-key',
            headers={'Content-Type': 'application/json',
                     'Authorization': 'Bearer {}'.format(auth_token)},
            data={'uuid': uuid}
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.text
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


# use the [device] API
def get_state_of_device(supervisor_address, balena_supervisor_api_key):
    """
    Returns the current device state, as reported to the balenaCloud API and with some extra fields added to allow
    control over pending/locked updates. The state is a JSON object that contains some or all of the following:

    :param supervisor_address: The full address for the API, i.e. "http://127.0.0.1:48484"
    :param balena_supervisor_api_key: Apikey parameter, which is exposed to the application
    """
    print("get state of device")
    try:
        response = requests.get(
            '/'.join([supervisor_address, 'v1/device']),
            params={'apikey': balena_supervisor_api_key},
            headers={'Content-Type': 'application/json'},
        )
        print("request finish")
        if response.status_code != 200:
            # raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
            raise VisoRequestError('get state of device', response.status_code, 'Failed')
        print("request OK")
        return response.text
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


def get_state_of_device_remotely(auth_token, uuid):
    """
    Returns the current device state, as reported to the balenaCloud API and with some extra fields added to allow
    control over pending/locked updates. The state is a JSON object that contains some or all of the following:

    :param auth_token: Auth Token
    :param uuid: Device ID
    """
    try:
        response = requests.post(
            'https://api.balena-cloud.com/supervisor/v1/device',
            headers={'Content-Type': 'application/json',
                     'Authorization': 'Bearer {}'.format(auth_token)},
            data={'uuid': uuid, 'method': 'GET'}
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.content
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()
