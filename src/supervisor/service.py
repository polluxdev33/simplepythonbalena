import requests
import inspect
from utils.common import logger
from supervisor.exception.viso_agent_exception import VisoRequestError


# use the [POST /v2/applications/:appId/restart-service, stop-service, start-service] API
def switch_single_service(supervisor_address, balena_supervisor_api_key, app_id, service_data, switch_action):
    """
    Use this endpoint to restart a service in the application with application id passed in with the url.

    :param supervisor_address:
    :param balena_supervisor_api_key:
    :param app_id:
    :param service_data: Information of service such as [service name] or [image ID]
        i.e {"serviceName": "my-service"} , {"imageId": 1234}
    :param switch_action: Action to switch the service such as [start], [stop], [restart]
    """
    try:
        if switch_action not in ['start', 'stop', 'restart']:
            return 'Error : Invalid Switch Action Type'
        url = '/'.join([supervisor_address, 'v2/applications', app_id, switch_action + '-service'])
        print(url)
        response = requests.get(
            url,
            params={'apikey': balena_supervisor_api_key},
            headers={'Content-Type': 'application/json'},
            data=service_data
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.text
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


def restart_all_service(supervisor_address, balena_supervisor_api_key, app_id):
    """
        Use this endpoint to restart every service in an application.
    """
    try:
        response = requests.post(
            '/'.join([supervisor_address, 'v2/applications', app_id, 'restart']),
            params={'apikey': balena_supervisor_api_key},
            headers={'Content-Type': 'application/json'},
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.text
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


def purge_data(supervisor_address, balena_supervisor_api_key, app_id):
    """
        Use this endpoint to purge all user data for a given application id.
    """
    try:
        response = requests.post(
            '/'.join([supervisor_address, 'v2/applications', app_id, 'purge']),
            params={'apikey': balena_supervisor_api_key},
            headers={'Content-Type': 'application/json'},
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.text
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


def get_supervisor_version(supervisor_address, balena_supervisor_api_key):
    """
        This endpoint returns the supervisor version currently running the device api.
    """
    try:
        response = requests.post(
            '/'.join([supervisor_address, 'v2/version']),
            params={'apikey': balena_supervisor_api_key},
            headers={'Content-Type': 'application/json'},
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.text
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()


def get_container_id(supervisor_address, balena_supervisor_api_key):
    """
        Use this endpoint to match a service name to a container ID.
    """
    try:
        response = requests.post(
            '/'.join([supervisor_address, 'v2/containerId']),
            params={'apikey': balena_supervisor_api_key},
            headers={'Content-Type': 'application/json'},
        )
        if response.status_code != 200:
            raise VisoRequestError(inspect.stack()[0][3], response.status_code, 'Failed')
        return response.text
    except VisoRequestError as err:
        err.log()
        return False
    except Exception as err:
        VisoRequestError(inspect.stack()[0][3], err).log()
