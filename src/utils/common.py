"""
    Common Utilities with various useful functions.
"""
import os
import signal
import subprocess
import logging.config
import pwd
from getmac import get_mac_address
import netifaces

_cur_dir = os.path.dirname(os.path.realpath(__file__))

logging.config.fileConfig(os.path.join(_cur_dir, 'logging.ini'))
logger = logging.getLogger('VA')


def get_net_interface():
    return netifaces.interfaces()[1]


def get_mac_address_from_boards():
    """
        Get MAC Address of the current board network adapter running this agent.
        :return:
        """
    interface = get_net_interface()
    str_list = interface[1].split("'")
    if len(str_list) == 1:
        net_interface = str_list[0]
    else:
        net_interface = str_list[1]
    mac_address = get_mac_address(interface=net_interface)
    return mac_address


def get_board_type():
    """
    Get type of the current board running this agent.
    :return:
    """
    # TODO: Add more boards here
    if 'arm' in os.uname()[4]:
        return 'rpi'


def get_serial():
    """
    Get serial number of the device
    :return:
    """
    board_type = get_board_type()
    if board_type == 'rpi':
        cpuserial = "0000000000000000"
        f = open('/proc/cpuinfo', 'r')
        for line in f:
            if line[0:6] == 'Serial':
                cpuserial = line[10:26].lstrip('0')
        f.close()
        return cpuserial
    # TODO: Add more board types
    else:
        return '12345678'


def check_running_proc(proc_name):
    """
    Check if a process is running or not
    :param proc_name:
    :return:
    """
    try:
        if len(os.popen("ps -aef | grep -i '%s' "
                        "| grep -v 'grep' | awk '{ print $3 }'" % proc_name).read().strip().splitlines()) > 0:
            return True
    except Exception as e:
        logger.error('Failed to get status of the process({}) - {}'.format(proc_name, e))
    return False


def kill_process_by_name(proc_name):
    """
    Kill process by its name
    :param proc_name:
    :return:
    """
    p = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE)
    out, err = p.communicate()
    for line in out.decode().splitlines():
        if proc_name in line:
            pid = int(line.split(None, 1)[0])
            print('Found PID({}) of `{}`, killing...'.format(pid, proc_name))
            os.kill(pid, signal.SIGKILL)


def get_cpu_temperature():
    board_type = get_board_type()
    if board_type == 'rpi':
        temp_file = '/sys/class/thermal/thermal_zone0/temp'
        return int(open(temp_file, 'r').read()) / 1000.
    else:
        return 36.5


def get_gpu_temperature():
    board_type = get_board_type()
    if board_type == 'rpi':
        temp = os.popen("vcgencmd measure_temp").readline()
        return float(temp.split('=')[1].split('\'C')[0])
    else:
        return 36.5


def get_screen_resolution():
    """
    Get resolution of the screen
    :return:
    """
    board_type = get_board_type()
    if board_type == 'rpi':
        pipe = os.popen('fbset -s')
        data = pipe.read().strip()
        pipe.close()
        for line in data.splitlines():
            if line.startswith('mode'):
                w, h = [int(p) for p in line.split('"')[1].split('x')]
                return w, h
    else:
        return 1920, 1080


def get_mem():
    """
    Get memory usage
    """
    pipe = os.popen("free -tm | grep 'Total' | awk '{print $2,$3,$4}'")
    data = pipe.read().strip().split()
    pipe.close()

    all_mem = int(data[0])
    used_mem = int(data[1])
    free_mem = int(data[2])

    percent = round(used_mem * 100. / all_mem, 1)

    return {'used': used_mem, 'total': all_mem, 'free': free_mem, 'percent': percent}


def get_up_time():
    pipe = os.popen("uptime")
    data = pipe.read().strip().split(',')[0]
    pipe.close()
    return ' '.join(data.split()[-2:])


def get_username():
    return pwd.getpwuid(os.getuid())[0]
